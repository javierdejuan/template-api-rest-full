<?php
	//como medida de seguridad básica, vamos a limpiar todas las variables por defecto
	
	//con esto prevenimos el XSS
	require_once $_SERVER['DOCUMENT_ROOT'].'/lib/htmlpurifier/HTMLPurifier.auto.php';
	// Configuración básica
	$config = HTMLPurifier_Config::createDefault();
	$config->set('Core.Encoding', CODIFICATION);
	$config->set('HTML.Doctype', 'HTML 4.01 Transitional');
	// Creamos la whitelist
	$config->set('HTML.Allowed', ALLOWED_HTML_TAGS); // configuramos las etiquetas que permitimos
	$sanitiser = new HTMLPurifier($config);
	
	//aquí limpiamos todas las variables y combatimos de manera superficial el sql inject
	$_REQUEST = sanitize($_REQUEST, $sanitiser);
	$_POST 	  = sanitize($_POST, 	$sanitiser);
	$_GET 	  = sanitize($_GET, 	$sanitiser);
	$_COOKIE  = sanitize($_COOKIE, 	$sanitiser, true);
	
	//libero memoria
	unset($sanitiser);