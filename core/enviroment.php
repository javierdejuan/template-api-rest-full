<?php

    //método que hacer requires de las clases automáticamente
    spl_autoload_register(function($class_name){

        //class directories
        $directories = array(
             '/classes/'
            ,'/classes/Bd/'
            ,'/classes/Mailing/'
            ,'/classes/Security/'
            ,'/api_methods/Documentation/'
            ,'/api_methods/Leads/'
			,'/api_methods/Contactos/'
			,'/api_methods/Sugerencias/'
			,'/api_methods/Identificacion/'
        );

        //for each directory
        foreach($directories as $directory) {
            //see if the file exsists
            if(file_exists($_SERVER['DOCUMENT_ROOT'].$directory."class.".strtolower($class_name). '.php')) {
                require_once($_SERVER['DOCUMENT_ROOT'].$directory."class.".strtolower($class_name). '.php');
                //only require the class once, so quit after to save effort (if you got more, then name them something else
                return;
            }
        }
    });

    // establezco la desviación horaria
    date_default_timezone_set('Europe/Madrid');

    // Fuerzo las cabeceras con la codificación y que no haya caché
    header('Content-Type: text/html; charset='.CODIFICATION);
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Pragma: no-cache");

    //variables útiles
    $db         = new Database();    //creo la conexión a la base de datos
    $access     = new Access($db);   //clase que se encarga del acceso a la api