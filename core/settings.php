<?php
    require_once __DIR__.'/../config/presets.php';  //variables y constantes de la aplicación
	require_once __DIR__.'/functions.php';          //métodos útiles de cara al desarrollo
	require_once __DIR__.'/security.php';           //medidas de seguridad (de todos los niveles)
    require_once __DIR__.'/enviroment.php';         //configuración y ayuda del entorno
    require_once __DIR__.'/../lib/Slim/Slim.php';   //framework api rest
    require_once __DIR__.'/../common/response.php'; //Métodos de respuesta