<?php
	function get_error($code){
		$errors = array(
			"0" => array(
                 "status_code" 	=> 403
                ,"error" 		=> true
                ,"message" 		=> "You are not allowed to use our API."
                ,"error_code"	=> 1990
            ),
            "1" => array(
                 "status_code" 	=> 412
                ,"error" 		=> true
                ,"message" 		=> "The Next parameters are required and were not received: [PARAMS]."
                ,"error_code"	=> 1991
            ),
            "2" => array(
                 "status_code" 	=> 200
                ,"error" 		=> true
                ,"message" 		=> "No se ha podido insertar el contacto."
                ,"error_code"	=> 1992
            ),
            "3" => array(
                 "status_code" 	=> 200
                ,"error" 		=> true
                ,"message" 		=> "No se ha podido insertar el lead."
                ,"error_code"	=> 1993
            ),
            "4" => array(
                 "status_code" 	=> 200
                ,"error" 		=> true
                ,"message" 		=> "No se ha podido enviar el email."
                ,"error_code"	=> 1994
            ),
            "5" => array(
                "status_code" 	=> 200
                ,"error" 		=> true
                ,"message" 		=> "C�digo de proovedor no v�lido."
                ,"error_code"	=> 1995
            ),
            "6" => array(
                "status_code" 	=> 200
                ,"error" 		=> true
                ,"message" 		=> "No se ha podido insertar la sugerencia."
                ,"error_code"	=> 1996
            )
		);
		return json_encode(utf8_converter($errors[$code]));
	}


    function get_success_msg($string){
        return json_encode(
            array(
                 "error" 		=> false
                ,"msg" 			=> utf8_encode($string)
            ));
    }

    function get_success_data($data){
        return json_encode(
            array(
                 "error" 		=> false
                ,"status_code" 	=> 200
                ,"data" 		=> $data
            ));
    }

    function get_external_error($string){
        return json_encode(
            array(
                 "error" 		=> true
                ,"msg" 			=> utf8_encode($string)
                ,"error_code"	=> 906
        ));
    }