<?php
require_once("class.phpmailer.php");

class Mailer{
           
	function enviar ($Mail){

		//configuracion
		 $mail               = new PHPMailer();
	    $mail->IsSMTP();
	    $mail->Host        = "52.50.119.35";
	    $mail->Port        = 25;
	    $mail->SMTPAuth    = false;
		$mail->Mailer 	   = "smtp";
		
		//puede que queramos cambiar el remitente por defecto
		if(!is_null($Mail->From) && $Mail->From != ''){
			$mail->From    = $Mail->From;
		}else{
			$mail->From	   = "administrador@icemd.com";
		}
		if(!is_null($Mail->FromName) && $Mail->FromName != ''){
			$mail ->FromName   = $Mail->FromName;
		}else{
			$mail->FromName	   = "ICEMD";
		}
				
		if($Mail->para!=""){
			$arr_paras = explode (";", $Mail->para);
			if(sizeof($arr_paras)>0){
				foreach ($arr_paras as $valor){
					if(trim($valor)!=""){
						$mail ->AddAddress (trim($valor));
					}
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
		
		$mail->IsHTML(true);
		$mail->Timeout = 120;
		$mail->Subject = utf2iso($Mail->asunto);
		$mail->Body    = utf2iso($Mail->cuerpo);
		$mail->AddReplyTo($mail ->From, $mail ->FromName);		
		
			
		//enviamos el mail. Si falla, lo intentamos enviar hasta 3 veces  
		$intentos   = 0;
		$ok 		= false;
		do{
			$ok = $mail ->Send();
			$intentos = $intentos+1;
		}while ( !$ok && $intentos < 3 );
		
		return $ok;
	}
}
?>
