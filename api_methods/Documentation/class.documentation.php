<?php
    class Documentation {

        public function home(){
            require_once "/dist/home.php";
        }

        /**
         *  @description: Método que genera la documentación de la API
         */
        public function get() {
            global $app;
            $documentation = array();

            //obtengo todos los métodos que usa la Api
            $router   = $app->router();
            $routes   = $router->getAllRoutes();

            foreach($routes as $route) {
                //obtengo la información del método de la clase que se encarga de la función de la API
                $method     = new ReflectionMethod($route['class'], $route['method']);

                //relleno la información que me da el PHPdoc
                $api_method = array(
                     "url"    => $route['url']
                    ,"method" => $route['http_method']
                    ,"desc"   => getDocComment($method->getDocComment(), "@description")
                    ,"params" => getDocComment($method->getDocComment(), "@param")
                    ,"title"  => getDocComment($method->getDocComment(), "@title")
                );

                if(!empty($api_method['params'])) {
                    $params               = (is_array($api_method['params']))?$api_method['params']:array($api_method['params']);
                    $api_method['params'] = array();
                    foreach ($params as $key => $value) {
                        $api_method['params'][$key] = explode("|", utf8_encode(str_replace("'", '', $value)));
                    }
                }

                if(!in_array($route['url'], array("/","/doc/"))) {
                    //genero una posicion del array por área
                    if(!array_key_exists($route['class'], $documentation))
                        $documentation[$route['class']] = array();

                    //meto el método dentro de su área
                    array_push($documentation[$route['class']], $api_method);
                }
            }

            require_once "/dist/index.php";
        }
    }