<?php
    $hostname 	= strtolower(gethostname());
    $lander 	= "/lander/i";
    $frontpre 	= "/frontpre/i";

    //develpment potencia el debugeo de la aplicación y configura cosas como el envío de emails
    $development = array(
         'enabled'			=> true
        ,'development_mail'	=> "alberto@lander.es"
        ,'querys'			=> true
        ,'query_params'		=> true
        ,'query_result'		=> false
    );

    if (preg_match($lander, $hostname)){
        define("ENVIROMENT",    "dev");
        $development['enabled'] = true;
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    }else if (preg_match($frontpre, $hostname)){
        define("ENVIROMENT",    "pre");
        $development['enabled'] = true;
    }else{
        define("ENVIROMENT",    "pro");
        $development['enabled'] = false;
    }

    $array_ini = parse_ini_file($_SERVER['DOCUMENT_ROOT']."/config/config_".ENVIROMENT.".ini");

    //definimos las constantes de aplicación
    foreach($array_ini as $ini_key=>$ini_value){
        define($ini_key, $ini_value);
    }

    define("DEVELOPMENT", serialize($development));
	
	//carpeta temporal para hacer cosas
	define("TMP_DIR", "tmp");

    //etiquetas permitidas a la hora de que los campos puedan tener código html (para proteger de xss)
    define("ALLOWED_HTML_TAGS",
        implode(",",
            array(
                "p"
                ,"b"
                ,"a[href]"
                ,"i"
                ,"ul"
                ,"li"
                ,"br"
            )
        )
    );