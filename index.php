<?php
	require_once __DIR__.'/core/settings.php'; //cerebro del sistema

	//iniciamos la api
	\Slim\Slim::registerAutoloader();
	$app 	 = new \Slim\Slim();

    //Métodos declarados de la api
    include_once __DIR__."/api_methods/features.php";

    //si hay acceso, arrancamos la api
    if($access->is_authorized($app, $db) && $access->required_params_received($app))
        $app->run();